import os
import camelot

def main():
	total = 0
	for file in os.listdir('pdfs'):
		if file.endswith('.pdf'):
			# Converts PDF into Python DataFrame
			tables = camelot.read_pdf(os.path.join('pdfs', file), flavor='lattice')
			print(tables)
			df = tables[0].df
			print(df)

			# Searches row position
			row_position = df.iloc[:, 3] == 'TOTAL'

			# Retrieves value
			value = df.loc[row_position, 4].values[0]

			# Increments total by new value
			total = total + float(value)

	# Displays calculated total value (with two decimals)
	print('TOTAL: £{}'.format(round(total, 2)))

# Defines behaviour for standalone application
if __name__ == '__main__':
	main()