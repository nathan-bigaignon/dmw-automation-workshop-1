import os


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'development_secret'
    UPLOAD_FOLDER = 'pdfs'
