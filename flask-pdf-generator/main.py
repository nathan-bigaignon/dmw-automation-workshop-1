import os
from flask import Flask, request, url_for, flash, redirect
from flask import render_template
from forms import GenerateForm
from config import Config
from weasyprint import HTML
from faker import Faker
from faker.providers import date_time


app = Flask(__name__, static_url_path='/static')
app.config.from_object(Config)


@app.route("/", methods=["POST", "GET"])
def generate():
    form = GenerateForm()
    if request.method == "GET":
        return render_template("home.html", form=form)
    if form.validate_on_submit():
        upload_path = os.path.join(app.root_path, app.config['UPLOAD_FOLDER'])
        fake = Faker()
        fake.add_provider(date_time)
        number_pdf = form.number_pdf.data
        for i in range(0, number_pdf):
            i = i + 1
            data = []
            total = 0
            for y in range(0, 10):
                y = y + 1
                amount = fake.pydecimal(left_digits=3, right_digits=2, positive=True)
                total = total + amount
                data.append({
                    "id": str(y),
                    "reference": str(fake.uuid4())[:8],
                    "description": fake.sentence(nb_words=12),
                    "date": str(fake.future_date()),
                    "amount": str(amount)
                })
            html_string = render_template('pdf.html', data=data, total=total)
            html_template = HTML(string=html_string, base_url=request.base_url)
            html_template.write_pdf('{}/invoice-{}.pdf'.format(upload_path, i),
                                    stylesheets=['static/css/vendors/bootstrap.min.css',
                                                 'static/css/pdf.css'])
        flash('{} PDFs generated in {}'.format(number_pdf, upload_path))
        return redirect(url_for('generate'))
    else:
        flash('An unexpected error has occurred. Please re-try later.')
        return redirect(url_for('generate'))
