from flask_wtf import FlaskForm
from wtforms import IntegerField, SubmitField
from wtforms.validators import DataRequired


class GenerateForm(FlaskForm):
	number_pdf = IntegerField(validators=[DataRequired()])
	submit = SubmitField('Generate')
